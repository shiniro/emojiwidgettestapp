//
//  EmojiWidgetBundle.swift
//  Emojibook WidgetExtension
//
//  Created by Julie PO on 2020/10/08.
//

import Foundation
import SwiftUI
import WidgetKit

@main
struct EmojiWidgetBundle: WidgetBundle {

  @WidgetBundleBuilder
  var body: some Widget {
    RandomEmojiWidget()
    CustomEmojiWidget()
  }
}
