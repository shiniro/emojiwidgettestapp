//
//  EmojiBookApp.swift
//  EmojiBook
//
//  Created by Julie PO on 2020/10/08.
//

import SwiftUI

@main
struct EmojiBookApp: App {
    var body: some Scene {
        WindowGroup {
            EmojibookListView()
        }
    }
}
